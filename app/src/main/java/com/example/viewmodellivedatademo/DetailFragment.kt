package com.example.viewmodellivedatademo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.viewmodellivedatademo.databinding.FragmentDetailBinding

class DetailFragment : Fragment() {

    private val mItemViewModel: ItemViewModel by activityViewModels()
    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mItemViewModel.numberOfCafe.observe(viewLifecycleOwner) {
            it?.let {
                binding.tvDetailCafeNumber.text = getString(R.string.number_of_cafe, it)
            }
        }
        mItemViewModel.numberOfJuice.observe(viewLifecycleOwner){
            it?.let {
                binding.tvDetailJuiceNumber.text = getString(R.string.number_of_juice,it)
            }
        }
        mItemViewModel.numberOfCake.observe(viewLifecycleOwner) {
            it?.let {
                binding.tvDetailCakeNumber.text = getString(R.string.number_of_cake, it)
            }
        }
    }

}