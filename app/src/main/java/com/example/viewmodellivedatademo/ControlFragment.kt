package com.example.viewmodellivedatademo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.viewmodellivedatademo.databinding.FragmentControlBinding

class ControlFragment : Fragment(), View.OnClickListener {

    private val mItemViewModel: ItemViewModel by activityViewModels()
    private var _binding: FragmentControlBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentControlBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        displayItem()

        binding.btnCafePlus.setOnClickListener(this)
        binding.btnJuicePlus.setOnClickListener(this)
        binding.btnCakePlus.setOnClickListener(this)
        binding.btnCafeMinus.setOnClickListener(this)
        binding.btnJuiceMinus.setOnClickListener(this)
        binding.btnCakeMinus.setOnClickListener(this)
    }

    private fun displayItem() {
        mItemViewModel.numberOfCafe.observe(viewLifecycleOwner) {
            it?.let {
                binding.tvMainCafeNumber.text = getString(R.string.number_of_cafe, it)
            }
        }
        mItemViewModel.numberOfJuice.observe(viewLifecycleOwner){
            it?.let {
                binding.tvMainJuiceNumber.text = getString(R.string.number_of_juice,it)
            }
        }
        mItemViewModel.numberOfCake.observe(viewLifecycleOwner) {
            it?.let {
                binding.tvMainCakeNumber.text = getString(R.string.number_of_cake, it)
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            binding.btnCafePlus.id -> {
                mItemViewModel.addCafeItemNumber()
                displayItem()
            }

            binding.btnJuicePlus.id -> {
                mItemViewModel.addJuiceItemNumber()
                displayItem()
            }

            binding.btnCakePlus.id -> {
                mItemViewModel.addCakeItemNumber()
                displayItem()
            }

            binding.btnCafeMinus.id -> {
                mItemViewModel.minusCafeItemNumber()
                displayItem()
            }

            binding.btnJuiceMinus.id -> {
                mItemViewModel.minusJuiceItemNumber()
                displayItem()
            }

            else -> {
                mItemViewModel.minusCakeItemNumber()
                displayItem()
            }
        }
    }
}