package com.example.viewmodellivedatademo

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ItemViewModel : ViewModel() {
    private var _numberOfCafe: MutableLiveData<Int> = MutableLiveData()
    private var _numberOfJuice: MutableLiveData<Int> = MutableLiveData()
    private var _numberOfCake: MutableLiveData<Int> = MutableLiveData()
    private var _totalItem: MutableLiveData<Int> = MutableLiveData()

    init {
        _numberOfCafe.value = 0
        _numberOfJuice.value = 0
        _numberOfCake.value = 0
        _totalItem.value = _numberOfCafe.value!! + _numberOfJuice.value!! + _numberOfCake.value!!
    }

    val numberOfCafe: LiveData<Int>
        get() = _numberOfCafe

    val numberOfJuice: LiveData<Int>
        get() = _numberOfJuice

    val numberOfCake: LiveData<Int>
        get() = _numberOfCake

    val totalItem: LiveData<Int>
        get() = _totalItem


    fun addCafeItemNumber() {
        _numberOfCafe.value = _numberOfCafe.value?.plus(1)
        _totalItem.value = _totalItem.value?.plus(1)
    }
    fun addJuiceItemNumber() {
        _numberOfJuice.value = _numberOfJuice.value?.plus(1)
        _totalItem.value = _totalItem.value?.plus(1)
    }

    fun addCakeItemNumber() {
        _numberOfCake.value = _numberOfCake.value?.plus(1)
        _totalItem.value = _totalItem.value?.plus(1)
    }

    fun minusCafeItemNumber() {
        if (_numberOfCafe.value!! > 0) {
            _numberOfCafe.value = _numberOfCafe.value?.minus(1)
        }
        if(_totalItem.value!!>0){
            _totalItem.value = _totalItem.value?.minus(1)
        }
    }
    fun minusJuiceItemNumber() {
        if (_numberOfJuice.value!! > 0) {
            _numberOfJuice.value = _numberOfJuice.value?.minus(1)
        }
        if(_totalItem.value!!>0){
            _totalItem.value = _totalItem.value?.minus(1)
        }
    }

    fun minusCakeItemNumber() {
        if (_numberOfCake.value!! > 0) {
            _numberOfCake.value = _numberOfCake.value?.minus(1)
        }
        if(_totalItem.value!!>0){
            _totalItem.value = _totalItem.value?.minus(1)
        }
    }

}