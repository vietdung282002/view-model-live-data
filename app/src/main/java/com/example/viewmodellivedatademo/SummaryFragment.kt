package com.example.viewmodellivedatademo

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.viewmodellivedatademo.databinding.FragmentSummaryBinding

class SummaryFragment : Fragment() {

    private val mItemViewModel: ItemViewModel by activityViewModels()
    private var _binding: FragmentSummaryBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentSummaryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mItemViewModel.totalItem.observe(viewLifecycleOwner) {
            it?.let {
                binding.tvNumberOfItem.text = activity?.getString(R.string.Total, it)
            }

        }
    }

}